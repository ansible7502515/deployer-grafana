# usilo-prometheus

Ce repos permet de deployer prometheus et identifier les outils à scraper

## Description du produit 

L'objectif de cette solution de monitoring est de fournir une surveillance centralisée et une visualisation des métriques système, performance, sécurité...

## Structure du repo

```text

usilo-prometheus
├── prod
│   ├── inventory.yaml
|   ├── secrets.yaml
│   └── variables.yaml
├── staging
│   ├── inventory.yaml
|   ├── secrets.yaml
│   └── variables.yaml
├── templates
│   ├── docker-compose.yml.j2
│   └── prometheus.yml.j2 
└── deploy.yaml

```

- **prod :** Ce dossier contient les fichiers d'inventaire et de variables pour déployer Prometheus en environnement de production

- **staging :** Ce dossier contient les fichiers d'inventaire et de variables pour déployer Prometheus en environnement de DEV
- **inventory.yaml :** Ce fichier est utilisé par Ansible ,il contient les hôtes des machines cibles

- **variables.yaml :** Ce fichier est également utilisé par Ansible pour stocker des variables spécifiques au déploiement de Prometheus. Il peut inclure des paramètres tels que les versions de SonarQube à déployer, les noms des bases de données à utiliser, les chemins de fichiers, etc. Les variables définies dans ce fichier sont utilisées pour rendre les playbooks Ansible plus flexibles et personnalisables pour différents environnements.

- **docker-compose.yaml.j2 :** Modèle de fichier docker-compose.yaml pour le déploiement de Prometheus avec Docker dans l'environnement de staging ou de production.

- **prometheus.yml.j2 :** Modèle de fichier prometheus.yml pour la les sources à scraper

- **deploy.yml :** C'est un fichier playbook Ansible, c'est-à-dire un script contenant une série d'instructions pour déployer Prometheus sur la machine spécifiée dans le fichier 
d'inventaire (inventory.yaml). Ce playbook exécutera les tâches nécessaires pour installer et configurer Semaphore sur les serveurs cibles en utilisant les variables spécifiées dans le fichier variables.yaml.

- **secrets.yaml :** Ce fichier est également utilisé par Ansible pour stocker les secrets utilisés lors du déploiement de Semaphore

- commande de playbook   ansible-playbook -i staging/inventory.yaml deploy.yaml  -u root -e ENVIRONNEMENT=staging
